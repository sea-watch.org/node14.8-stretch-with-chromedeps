FROM docker.io/node:16-bullseye

RUN apt-get update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -y \
        libnss3 libatk-bridge2. libx11-xcb1 libxcb-dri3-0 libdrm2 libgbm1 libasound2 libxss1 libgtk-3-0
